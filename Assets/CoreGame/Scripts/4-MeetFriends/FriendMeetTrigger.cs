﻿using UnityEngine;
using System.Collections;
using WellFired;

public class FriendMeetTrigger : MonoBehaviour
{
    public USSequencer friendDiscoverPlayerSequence;
    public USSequencer friendApproachPlayerSequence;
    public USSequencer friendGreetPlayerSequence;
    public USTimelineContainer friendApproachPlayerSequence_PlayerAffectedObjectContainer;
    private EnemyStateNotifier enemyStateNotifierRef;
    private LevelLogic levelLogicRef;
    public int numberOfEnemiesOnMap;

    // Use this for initialization
    void Start()
    {
        levelLogicRef = GameObject.FindObjectOfType<LevelLogic>();
        levelLogicRef.LevelPieceTransitionFinish += OnLevelPieceTransitionFinish;

        enemyStateNotifierRef = levelLogicRef.GetComponent<EnemyStateNotifier>();
        enemyStateNotifierRef.EnemyDefeatedEvent += enemyStateNotifierRef_EnemyDefeatedEvent;


        friendDiscoverPlayerSequence.Play();
    }

    /// <summary>
    /// Gathers the number of enemies as soon as the level transitions conclude.
    /// This is to avoid gathering existing enemies in another level piece.
    /// Alternatively we can simply check out level piece root GameObject,
    /// but this might add a bit more flexibility?
    /// </summary>
    private void OnLevelPieceTransitionFinish()
    {
        levelLogicRef.LevelPieceTransitionFinish -= OnLevelPieceTransitionFinish;
        numberOfEnemiesOnMap = GameObject.FindGameObjectsWithTag("Enemy").Length;
    }

    void OnDestroy()
    {
        enemyStateNotifierRef.EnemyDefeatedEvent -= enemyStateNotifierRef_EnemyDefeatedEvent;
    }

    void enemyStateNotifierRef_EnemyDefeatedEvent(GameObject enemyObject)
    {
        --numberOfEnemiesOnMap;

        // We have defeated all the enemies.
        if( numberOfEnemiesOnMap <= 0 )
        {
            // Since we move the player from piece to piece, the sequence may not have
            // an accurate reference. Let's update the reference, so the sequence can 
            // correctly manipulate the player.
            friendApproachPlayerSequence_PlayerAffectedObjectContainer.AffectedObject =
                GameObject.FindGameObjectWithTag("Player").transform;


            friendApproachPlayerSequence.Play();

        }
    }

}
