using UnityEngine;
using System.Collections;

public class IntroDialogue : MonoBehaviour
{
    [SerializeField]
    private NarratorTextDisplayController narrator;

    // Use this for initialization
    void Start()
    {
        narrator.SequenceCompletedEvent += OnIntroTextFinish;
        narrator.DisplaySequence();
    }

    private void OnIntroTextFinish()
    {
        GameObject.FindObjectOfType<LevelLogic>().AdvanceToNextMap();
    }
}
