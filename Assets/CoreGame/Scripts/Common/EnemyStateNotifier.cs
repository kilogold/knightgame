﻿using UnityEngine;
using System.Collections;

public class EnemyStateNotifier : MonoBehaviour 
{
    public delegate void EnemyDefeatedDelegate(GameObject enemyObject);
    public event EnemyDefeatedDelegate EnemyDefeatedEvent;

    public void NotifyEnemyDefeated( GameObject enemyObject )
    {
        //Ensure object parameter is indeed enemy
        if( enemyObject.tag == "Enemy")
        {
            EnemyDefeatedEvent(enemyObject);
        }
    }
}
