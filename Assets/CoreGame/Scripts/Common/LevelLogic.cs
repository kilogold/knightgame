using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public class LevelLogic : MonoBehaviour 
{
	/************************************
	 *  MEMBERS
	 ************************************/

	/// <summary>
	/// The transitioning objects that are intended to move
	/// from one map to another. These are kept in order to
	/// perform the transition between maps where each object
	/// is moved along the Y-component.
	/// </summary>
	public HashSet<Transform> transitioningObjects = new HashSet<Transform>();

    private string[] mapNamesArray = new string[] {
        "LevelFloorBG0",
        "LevelFloorBG1",
        "LevelFloorBG2",
        "LevelFloorBG3",
        "LevelFloorBG4",
        "LevelFloorBG5",
        "LevelFloorBG6",
        "LevelFloorBG7",
        "LevelFloorBG8",
        "LevelFloorBG9",
        "LevelFloorBG10",
        "LevelFloorBG11",
        "LevelFloorBG12",
    };
    
    [HideInInspector]
	public GameObject currentMap;
	private GameObject nextMap;
	private GameObject playerGameObject;
	
    public const float TRANSITION_SPEED = 6.0f;
    public const float NEXT_MAP_STARTING_OFFSET_Y = -7.62f;
	public bool isTransitioningToNextMap = false;

    public event Action LevelPieceTransitionFinish;


	/************************************
	 *  PROPERTIES
	 ************************************/
    public int CurrentLevel { get; private set; }

	/************************************
	 *  METHODS
	 ************************************/
	// Use this for initialization
	void Start () 
	{
        StartCoroutine(CoStart());
	}

    IEnumerator CoStart()
    {
        
        playerGameObject = GameObject.FindGameObjectWithTag("Player");
        transitioningObjects.Add(playerGameObject.transform);

        Application.LoadLevelAdditive(mapNamesArray[CurrentLevel]);

        yield return new WaitForEndOfFrame();

        string levelFloorName = mapNamesArray[CurrentLevel];
        currentMap = GameObject.Find(levelFloorName);

        yield return null;
    }

	public void AdvanceToNextMap()
	{
        StartCoroutine(CoAdvanceToNextMap());
	}
    private IEnumerator CoAdvanceToNextMap()
    {
        // We want to wait till we have loaded the new map in order to begin the transition.
        yield return StartCoroutine(CoLoadNextMap());
        yield return StartCoroutine(CoTransitionViewToNextMap());
    }

	private IEnumerator CoLoadNextMap()
	{
        ++CurrentLevel;
        Application.LoadLevelAdditive(mapNamesArray[CurrentLevel]);

        // Need to wait one frame until LoadLevelAdditive includes contents into the hierarchy.
        yield return null;

        // Collect the total amount of level pieces in the scene.
        // This includes both level piece replacements (cutscenes)
        // and actual level piece.
        GameObject[] activeLevelPieces = 
            GameObject.FindGameObjectsWithTag("LevelPiece");
        GameObject[] activeLevelPieceReplacements = 
            GameObject.FindGameObjectsWithTag("LevelPieceReplacement");

        int totalLevelPieceCount = activeLevelPieces.Length + activeLevelPieceReplacements.Length;
        List<GameObject> totalLevelPieces = new List<GameObject>(totalLevelPieceCount);
        totalLevelPieces.AddRange(activeLevelPieces);
        totalLevelPieces.AddRange(activeLevelPieceReplacements);

        // We should only ever have 2 at a time
        if( totalLevelPieces.Count != 2 )
        {
            Debug.LogException(new Exception("Attempting to load an odd amount of level pieces."));
        }

        // Since it's only 2 level pieces, let's avoid doing a loop
        // There's a chance both current and next maps have the same GameObject name.
        // If this is the case, we need to uniquely identify them. 
        // We do so by checking for the only other entry (via ref comparison)
        if (currentMap == totalLevelPieces[0])
        {
            nextMap = totalLevelPieces[1];
        }
        else
        {
            nextMap = totalLevelPieces[0];
        }

        float mapYoffset = currentMap.transform.position.y + NEXT_MAP_STARTING_OFFSET_Y;
        nextMap.transform.position += new Vector3(0, mapYoffset, 0);
	}

    private IEnumerator CoTransitionViewToNextMap()
    {
        isTransitioningToNextMap = true;
        {
            // Transition all the objects meant to transition, such as player.
            foreach (Transform transitionObj in transitioningObjects)
            {
                transitionObj.parent = nextMap.transform;
            }

            float src = NEXT_MAP_STARTING_OFFSET_Y;
            float dst = 0;
            float transitionTime = 0.50f;
            float offsetPerFrame = (dst - src) / transitionTime;
            float offsetPerFrameWithDeltaTime = offsetPerFrame * Time.deltaTime;

            float start = Time.time;
            while (nextMap.transform.position.y < 0)
            {
                currentMap.transform.Translate(0, offsetPerFrameWithDeltaTime, 0);
                nextMap.transform.Translate(0, offsetPerFrameWithDeltaTime, 0);
                yield return null;
            }
            float end = Time.time;
            Debug.Log("Transition occured in " + (end - start) + " seconds");


            // We are done transitioning...
            Destroy(currentMap);
            nextMap.transform.position = Vector3.zero;
            currentMap = nextMap;
            nextMap = null;

            // Allow the old level piece destruction to fully take place before proceeding.
            yield return null; 
        }
        isTransitioningToNextMap = false;

        if (null != LevelPieceTransitionFinish)
        {
            LevelPieceTransitionFinish();
        }
    }

    public bool DoesNextMapExist()
    {
        string filename = Application.dataPath + "/CoreGame/Scenes/Resources/" + mapNamesArray[CurrentLevel + 1] + ".unity";

        if ((CurrentLevel + 1) > mapNamesArray.Length)
        {
            Debug.Log("Requested level index is out of bounds. Level does not exist.");
            return false;
        }

        if( false == System.IO.File.Exists(filename) )
        {
            Debug.LogError(filename + " map scene file not found.");
            return false;
        }

        return true;
    }
}
