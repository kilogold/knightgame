﻿using UnityEngine;
using System.Collections;

public class PlayerCollisionResponses : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            Vector3 flinchOffsetDirection = (other.transform.position - transform.position).normalized;
            transform.position += -(flinchOffsetDirection);
        }
    }
}