﻿using UnityEngine;
using System.Collections;
using Behave.Runtime;
using Tree = Behave.Runtime.Tree;

public class EnemyFodderBehavior : BAKnightGameBehaveLibrary_EnemyFodderBlueprint
{
    public Transform targetTransform;
    public float attackRange;
    public float currentTreeTickTimer;
    public float maxTreeTickTimer;
    public Tree behaviourTree;
    public EnemyFodderPathfinding pathfinder;

    // Use this for initialization
    void Start()
    {
        // Instantiate AI tree
        behaviourTree = BLKnightGameBehaveLibrary.InstantiateTree(BLKnightGameBehaveLibrary.TreeType.EnemyFodderTree, this);
        maxTreeTickTimer = 1 / behaviourTree.Frequency;
    }

    // Update is called once per frame
    void Update()
    {
        // Tick the timer at the rate specified by the tree
        currentTreeTickTimer -= Time.deltaTime;
        if (currentTreeTickTimer <= 0)
        {
            // Reset the tree-tick timer
            currentTreeTickTimer = maxTreeTickTimer;

            // Tick the tree
            if (behaviourTree.Tick() != BehaveResult.Running)
            {
                behaviourTree.Reset();
            }
        }
    }

    public override BehaveResult TickApproachTargetAction(Tree sender)
    {
        pathfinder.GeneratePathToTarget(targetTransform.position);

        if( pathfinder.isAtEndOfPath )
        {
            return BehaveResult.Success;
        }
        else
        {
            pathfinder.StartContinuouslyTracking();
            return BehaveResult.Running;
        }
    }

    public override BehaveResult TickAcquireTargetAction(Tree sender)
    {
      GameObject playerRef = GameObject.FindGameObjectWithTag("Player");

      if( playerRef != null)
      {
        targetTransform = playerRef.transform;
        return BehaveResult.Success;
      }

      return BehaveResult.Failure;
    }
}
