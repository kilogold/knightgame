﻿using UnityEngine;
using System.Collections;

public class FollowHero : MonoBehaviour 
{
	public GameObject heroObjectRef;
	public float moveSpeed = 10;
	public float deadZone = 1.5f;

	// Use this for initialization
	void Start () 
	{
		heroObjectRef = GameObject.FindWithTag( "Player" );
	}
	
	// Update is called once per frame
	void Update () 
	{
		Vector3 selfToHero = heroObjectRef.transform.position - transform.position;

		if( selfToHero.magnitude > deadZone )
		{
			transform.position += (selfToHero.normalized * moveSpeed) * Time.deltaTime;
		}
	}
}
