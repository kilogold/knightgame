﻿using UnityEngine;
using System.Collections;

public class ActorAnimations : MonoBehaviour 
{
    public CoreDeclarations.DIRECTIONS lastFacingDirection;
    public Animator playerAnimator;

    public enum ActorAnimationState
    {
        Walking,
        Striking,
        Idle,
        MAX_STATES
    }

    private int[,] animationHashes;

    public ActorAnimationState CurrentAnimationState
    {
        get 
        {
            int stateInfoNameHash = playerAnimator.GetCurrentAnimatorStateInfo(0).fullPathHash;

            if( stateInfoNameHash == GetAnimationHash(ActorAnimationState.Idle,CoreDeclarations.DIRECTIONS.DOWN)  ||
                stateInfoNameHash == GetAnimationHash(ActorAnimationState.Idle,CoreDeclarations.DIRECTIONS.UP)    ||
                stateInfoNameHash == GetAnimationHash(ActorAnimationState.Idle,CoreDeclarations.DIRECTIONS.RIGHT) ||
                stateInfoNameHash == GetAnimationHash(ActorAnimationState.Idle,CoreDeclarations.DIRECTIONS.LEFT)  )
            {
                return ActorAnimationState.Idle;
            }

            if (stateInfoNameHash == GetAnimationHash(ActorAnimationState.Walking, CoreDeclarations.DIRECTIONS.DOWN)  ||
                stateInfoNameHash == GetAnimationHash(ActorAnimationState.Walking, CoreDeclarations.DIRECTIONS.UP)    ||
                stateInfoNameHash == GetAnimationHash(ActorAnimationState.Walking, CoreDeclarations.DIRECTIONS.RIGHT) ||
                stateInfoNameHash == GetAnimationHash(ActorAnimationState.Walking, CoreDeclarations.DIRECTIONS.LEFT)  )
            {
                return ActorAnimationState.Walking;
            }

            if (stateInfoNameHash == GetAnimationHash(ActorAnimationState.Striking, CoreDeclarations.DIRECTIONS.DOWN) ||
                stateInfoNameHash == GetAnimationHash(ActorAnimationState.Striking, CoreDeclarations.DIRECTIONS.UP) ||
                stateInfoNameHash == GetAnimationHash(ActorAnimationState.Striking, CoreDeclarations.DIRECTIONS.RIGHT) ||
                stateInfoNameHash == GetAnimationHash(ActorAnimationState.Striking, CoreDeclarations.DIRECTIONS.LEFT))
            {
                return ActorAnimationState.Striking;
            }

            Debug.LogError("No valid current animation state");
            return ActorAnimationState.MAX_STATES;
        }

    }

    protected void InitializeAnimationHashContainer( int numberOfAnimationStates )
    {
        animationHashes = new int[numberOfAnimationStates, (int)CoreDeclarations.DIRECTIONS.MAX_DIRECTIONS];
    }

    protected void SetAnimationHash(ActorAnimationState animationState, CoreDeclarations.DIRECTIONS animationDirection, string animationName)
    {
        animationHashes[(int)animationState, (int)animationDirection] = Animator.StringToHash(animationName);
    }
    protected int GetAnimationHash(ActorAnimationState animationState, CoreDeclarations.DIRECTIONS animationDirection)
    {
        return animationHashes[(int)animationState, (int)animationDirection];
    }

    void Start()
    {
        Debug.LogWarning("No initialization code implemented. No animations loaded on startup.");
    }

    public void SetWalkAnimation( Vector2 moveDir )
    {
        // Determine facing direction
        if (Mathf.Abs(moveDir.x) >= Mathf.Abs(moveDir.y))
        {
            if (moveDir.x > 0)
            {
                lastFacingDirection = CoreDeclarations.DIRECTIONS.RIGHT;
            }
            else
            {
                lastFacingDirection = CoreDeclarations.DIRECTIONS.LEFT;
            }
        }
        else
        {
            if (moveDir.y > 0)
            {
                lastFacingDirection = CoreDeclarations.DIRECTIONS.UP;
            }
            else
            {
                lastFacingDirection = CoreDeclarations.DIRECTIONS.DOWN;
            }
        }

        // Play walking animation
        playerAnimator.Play(GetAnimationHash(ActorAnimationState.Walking, lastFacingDirection));

    }

    public void SetIdleAnimation()
    {
        playerAnimator.Play(GetAnimationHash(ActorAnimationState.Idle, lastFacingDirection));
    }

    public void SetStrikingAnimation()
    {
        playerAnimator.Play(GetAnimationHash(ActorAnimationState.Striking, lastFacingDirection));
    }
}
