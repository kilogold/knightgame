﻿using UnityEngine;
using System.Collections;

public class MathUtilities
{
    public static float FullRangeRotationFromOriginToTarget( Vector3 origin, Vector3 target, out Vector3 normalizedAngle )
    {
        normalizedAngle = (target - origin).normalized;

        float zRot = Vector3.Angle(new Vector3(1, 0, 0), normalizedAngle);

        // counteract rotation when considering corner case of rotation generation.
        // the API only provides nearest angles (0 - 180 degrees).
        if (target.y < origin.y)
        {
            zRot = 360.0f - zRot;
        } 

        return zRot;
    }
}
