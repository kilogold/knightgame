﻿using UnityEngine;
using System.Collections;

public class BehaveDebugUpdater : MonoBehaviour
{
    public int updateFrequency;

    // Use this for initialization
    void OnEnable()
    {
        Behave.Debugger.DebugUpdater.Start(updateFrequency);
    }

    void OnDisable()
    {
        Behave.Debugger.DebugUpdater.Stop();
    }


}
