﻿using UnityEngine;
using System.Collections;

public class ActorSpriteCollisionSort : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D other)
    {
        if( gameObject.name.Contains("Sprite"))
        {
            // We are reacting to Sprite collider.
            GetComponent<SpriteRenderer>().sortingOrder = 5;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (gameObject.name.Contains("Sprite"))
        {
            // We are reacting to Sprite collider.
            GetComponent<SpriteRenderer>().sortingOrder = 1;
        }
    }
}
