﻿using UnityEngine;
using System.Collections;

public class PlayerAnimations : ActorAnimations 
{
    void Start()
    {
        lastFacingDirection = CoreDeclarations.DIRECTIONS.DOWN;
        InitializeAnimationHashContainer((int)ActorAnimationState.MAX_STATES);

        SetAnimationHash(ActorAnimationState.Walking, CoreDeclarations.DIRECTIONS.LEFT, "Base Layer.PlayerWalkLeft");
        SetAnimationHash(ActorAnimationState.Walking, CoreDeclarations.DIRECTIONS.DOWN, "Base Layer.PlayerWalkDown");
        SetAnimationHash(ActorAnimationState.Walking, CoreDeclarations.DIRECTIONS.UP, "Base Layer.PlayerWalkUp");
        SetAnimationHash(ActorAnimationState.Walking, CoreDeclarations.DIRECTIONS.RIGHT, "Base Layer.PlayerWalkRight");

        SetAnimationHash(ActorAnimationState.Idle, CoreDeclarations.DIRECTIONS.LEFT, "Base Layer.PlayerIdleLeft");
        SetAnimationHash(ActorAnimationState.Idle, CoreDeclarations.DIRECTIONS.DOWN, "Base Layer.PlayerIdleDown");
        SetAnimationHash(ActorAnimationState.Idle, CoreDeclarations.DIRECTIONS.UP, "Base Layer.PlayerIdleUp");
        SetAnimationHash(ActorAnimationState.Idle, CoreDeclarations.DIRECTIONS.RIGHT, "Base Layer.PlayerIdleRight");

        SetAnimationHash(ActorAnimationState.Striking, CoreDeclarations.DIRECTIONS.LEFT, "Base Layer.PlayerStrikeLeft");
        SetAnimationHash(ActorAnimationState.Striking, CoreDeclarations.DIRECTIONS.DOWN, "Base Layer.PlayerStrikeDown");
        SetAnimationHash(ActorAnimationState.Striking, CoreDeclarations.DIRECTIONS.UP, "Base Layer.PlayerStrikeUp");
        SetAnimationHash(ActorAnimationState.Striking, CoreDeclarations.DIRECTIONS.RIGHT, "Base Layer.PlayerStrikeRight");
    }
}
