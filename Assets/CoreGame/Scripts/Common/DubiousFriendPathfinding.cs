﻿using UnityEngine;
using System.Collections;
using Pathfinding;

[RequireComponent (typeof(Seeker))]
public class DubiousFriendPathfinding : MonoBehaviour
{
    private Seeker pathfindingSeeker;
    private ActorAnimations actorAnimations;

    //The calculated path
    public Path path;

    //The AI's speed per second
    public float speed = 0.005f;

    //The waypoint we are currently moving towards
    private int currentWaypoint = 0;

    //The max distance from the AI to a waypoint for it to continue to the next waypoint
    public float nextWaypointDistance = 1;

    public bool isAtEndOfPath = false;
    public bool isContinuouslyStepping = false;

    // Use this for initialization
    void Start()
    {
        pathfindingSeeker = GetComponent<Seeker>();
        actorAnimations = GetComponent<ActorAnimations>();
    }

    public void GeneratePathToTarget( Vector3 targetPosition )
    {
        currentWaypoint = 0;
        isAtEndOfPath = false;
        path = pathfindingSeeker.StartPath( transform.position, targetPosition );
    }

    public void StepTowardsTarget()
    {
        if (path == null)
        {
            //We have no path to move after yet
            return;
        }

        if (currentWaypoint >= path.vectorPath.Count)
        {
            actorAnimations.SetIdleAnimation();
            isAtEndOfPath = true;
            return;
        }

        //Check if we are close enough to the next waypoint
        //If we are, proceed to follow the next waypoint
        if (Vector3.Distance(transform.position, path.vectorPath[currentWaypoint]) < nextWaypointDistance)
        {
            currentWaypoint++;
            return;
        }

        // Set the directional movement.
        Vector3 moveDir = Vector3.zero;
        MathUtilities.FullRangeRotationFromOriginToTarget(
            transform.position,
            path.vectorPath[currentWaypoint],
            out moveDir );

        // Set the animation
        actorAnimations.SetWalkAnimation( new Vector2(moveDir.x,moveDir.y) );
        
        // Move in direction to the next waypoint
        transform.position += moveDir * speed * Time.fixedDeltaTime;    
    }

    public void StartContinuouslyTracking()
    {
        if (false == isContinuouslyStepping)
        {
            isContinuouslyStepping = true;
            StartCoroutine(ContinuouslyStepPath());
        }
    }

    public void StopContinuouslyTracking()
    {
        isContinuouslyStepping = false;
    }

    IEnumerator ContinuouslyStepPath()
    {
        while (isContinuouslyStepping)
        {
            StepTowardsTarget();
            yield return null;
        }
    }
}
