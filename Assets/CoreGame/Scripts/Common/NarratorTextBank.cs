using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NarratorTextBank : MonoBehaviour
{
    private const string narratorLinesFilepath = "DialogueMap";
    private Dictionary<string, string> narratorLines;

    public string GetNarratorLine(string dialogueID)
    {
		foreach (var item in narratorLines.Keys)
		{
			Debug.Log(item);
		} 

		Debug.Log("Cur key " + dialogueID);
        return narratorLines[dialogueID];
    }

    void Awake()
    {
        narratorLines = new Dictionary<string, string>();

        LoadNarratorLines(narratorLinesFilepath);
    }

    private void LoadNarratorLines( string filePath )
    {

        TextAsset textFile = Resources.Load<TextAsset>("DialogueMap");
        string[] textFileLines = textFile.ToString().Split('\n');

        string curLineHeader = string.Empty;
        string curLineContent = string.Empty;

        foreach (string line in textFileLines)
        {
            // We confirm the end of an entry by an empty new line
            if (string.IsNullOrEmpty(line))
            {
                // Load the entry into the NarratorLines
                narratorLines.Add(curLineHeader, curLineContent);

                //Clean the entry data
                curLineHeader = curLineContent = string.Empty;
                
                // Move onto the next iteration where the next 
                // dialogue entry begins.
                continue;
            }

            if(IsStringDialogueHeader(line))
            {
                // Obtain the header parsed info without the brackets
                curLineHeader = line;
            }
            else
            {
                // parse the content with a new line at the end
                // in case designers choose to make multi-line dialogue
                // or ASCII art.
                curLineContent += (line + "\n");
            }
        }
    }

    private bool IsStringDialogueHeader( string stringValue )
    {
        int startIndex = 0;
        int endIndex = stringValue.Length-1;

        if( stringValue[startIndex] == '[' && 
            stringValue[endIndex] == ']'  )
        {
            return true;
        }

        return false;
    }
}
