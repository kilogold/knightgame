﻿using UnityEngine;
using System.Collections;
using Behave.Runtime;
using Tree = Behave.Runtime.Tree;

[RequireComponent(typeof(DubiousFriendPathfinding))]

public class DubiousFriendBehavior : BAKnightGameBehaveLibrary_DubiousFriendBlueprint
{
    public Transform targetTransform;
    public float attackRange;
    public float currentTreeTickTimer;
    public float maxTreeTickTimer;
    public Tree behaviourTree;
    public DubiousFriendPathfinding pathfinder;

    // Use this for initialization
    void Start()
    {
        // When the friend comes into play, this actor must be saved
        // as transitional/persistent.
        GameObject.FindObjectOfType<LevelLogic>().transitioningObjects.Add(transform);

        // Instantiate AI tree
        behaviourTree = BLKnightGameBehaveLibrary.InstantiateTree(BLKnightGameBehaveLibrary.TreeType.DubiousFriendTree, this);
        maxTreeTickTimer = 1 / behaviourTree.Frequency;
    }

    // Update is called once per frame
    void Update()
    {
        // Tick the timer at the rate specified by the tree
        currentTreeTickTimer -= Time.deltaTime;
        if (currentTreeTickTimer <= 0)
        {
            // Reset the tree-tick timer
            currentTreeTickTimer = maxTreeTickTimer;

            // Tick the tree
            if (behaviourTree.Tick() != BehaveResult.Running)
            {
                behaviourTree.Reset();
            }
        }
    }

    public override BehaveResult TickApproachTargetAction(Tree sender)
    {
        pathfinder.GeneratePathToTarget(targetTransform.position);

        if (pathfinder.isAtEndOfPath)
        {
            return BehaveResult.Success;
        }
        else
        {
            pathfinder.StartContinuouslyTracking();
            return BehaveResult.Running;
        }
    }

    public override BehaveResult TickAcquireTargetAction(Tree sender)
    {
        // Get the list of all enemies
        GameObject[] enemiesArray = GameObject.FindGameObjectsWithTag("Enemy");

        // Cycle thorugh every enemy to get the nearest one.
        GameObject closestEnemy = null;
        float closestEnemyMagnitude = 0;

        foreach (GameObject curEnemy in enemiesArray)
        {
            if (closestEnemy == null)
            {
                closestEnemy = curEnemy;
            }
            else
            {
                float curEnemyDistance = (curEnemy.transform.position - transform.position).magnitude;

                if (curEnemyDistance < closestEnemyMagnitude)
                {
                    closestEnemyMagnitude = curEnemyDistance;
                    closestEnemy = curEnemy;
                }
            }
        }

        // If we didn't find an enemy, we should go back to following the player.
        if (closestEnemy == null)
        {
            targetTransform = GameObject.FindGameObjectWithTag("Player").transform;
        }
        // We found an enemy, we should now approach it.
        else
        {
            targetTransform = closestEnemy.transform;
        }

        return BehaveResult.Success;
    }

    public override BehaveResult TickValidateTargetAsEnemyAction(Tree sender)
    {
        if( targetTransform.gameObject.tag == "Enemy")
        {
            return BehaveResult.Success;
        }

        return BehaveResult.Failure;
    }
}
