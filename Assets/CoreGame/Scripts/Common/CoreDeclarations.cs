﻿using UnityEngine;
using System.Collections;

public class CoreDeclarations
{
    public enum DIRECTIONS
    {
        LEFT,
        RIGHT,
        UP,
        DOWN,
        MAX_DIRECTIONS,
        UNDEFINED
    }
}
