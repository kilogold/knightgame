using UnityEngine;
using System.Collections;

namespace WellFired
{
	[USequencerFriendlyName("Change Color (Sprite)")]
	[USequencerEvent("Render/Change Objects Sprite Color")]
	public class USChangeColorSprite : USEventBase 
	{	
		public Color newColor;
		private Color previousColor;
		
		public override void FireEvent()
		{	
			if(!AffectedObject)
				return;

			SpriteRenderer sprite = AffectedObject.GetComponent<SpriteRenderer>();
			previousColor = sprite.color;
			sprite.color = newColor;
		}
		
		public override void ProcessEvent(float deltaTime)
		{
			
		}
		
		public override void StopEvent()
		{
			UndoEvent();
		}
		
		public override void UndoEvent()
		{
			if(!AffectedObject)
				return;

			SpriteRenderer sprite = AffectedObject.GetComponent<SpriteRenderer>();
			sprite.color = previousColor;
		}
	}
}