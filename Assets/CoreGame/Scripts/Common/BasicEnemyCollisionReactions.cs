﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BasicEnemyCollisionReactions : MonoBehaviour
{
    private EnemyStateNotifier enemyStateNotifierRef;    
    private BasicEnemyStats enemyStats;

    void Start()
    {
        enemyStats = GetComponent<BasicEnemyStats>();

        GameObject levelLogicObject = GameObject.Find("LevelLogic");
        enemyStateNotifierRef = levelLogicObject.GetComponent<EnemyStateNotifier>();

        enemyStateNotifierRef.EnemyDefeatedEvent += BasicEnemyCollisionReactions_EnemyDefeatedEvent;
    }

    void OnDestroy()
    {
        enemyStateNotifierRef.EnemyDefeatedEvent -= BasicEnemyCollisionReactions_EnemyDefeatedEvent;
    }

    void BasicEnemyCollisionReactions_EnemyDefeatedEvent(GameObject enemyObject)
    {
        if( gameObject == enemyObject )
        {
            Destroy(enemyObject);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.name == "Sword")
        {
            enemyStats.currentHP--;

            if (enemyStats.currentHP <= 0)
            {
                // Notify everyone the enemy was defeated.
                // We could self-destruct, but then others would not
                // be informed of this event.
                enemyStateNotifierRef.NotifyEnemyDefeated(gameObject);
            }
            else
            {
                Vector3 flinchOffsetDirection = (other.transform.position - transform.position).normalized;
                transform.position += -(flinchOffsetDirection);
            }
        }
    }
}
