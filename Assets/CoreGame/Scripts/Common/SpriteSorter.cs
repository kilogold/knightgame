﻿using UnityEngine;
using System.Collections;

public class SpriteSorter : MonoBehaviour 
{
    public SpriteRenderer spriteRenderer;
    public int originalSortValue;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Ally")
        {
            SpriteRenderer otherSpriteRenderer = other.GetComponentInChildren<SpriteRenderer>();

            originalSortValue = spriteRenderer.sortingOrder;
            spriteRenderer.sortingOrder = otherSpriteRenderer.sortingOrder + 1;
        }
    }


    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Ally")
        {
            spriteRenderer.sortingOrder = originalSortValue;
        }
    }


}
