﻿using UnityEngine;
using System.Collections;
using Pathfinding;

[RequireComponent (typeof(Seeker))]
public class EnemyFodderPathfinding : MonoBehaviour
{
    private Seeker pathfindingSeeker;

    //The calculated path
    public Path path;

    //The AI's speed per second
    public float speed = 0.005f;

    //The waypoint we are currently moving towards
    private int currentWaypoint = 0;

    //The max distance from the AI to a waypoint for it to continue to the next waypoint
    public float nextWaypointDistance;

    public bool isAtEndOfPath = false;
    public bool isContinuouslyStepping = false;

    // Use this for initialization
    void Start()
    {
        // Set the waypoint distance relative to the size of the enemy.
        nextWaypointDistance = transform.localScale.x / 2;
        pathfindingSeeker = GetComponent<Seeker>();
    }

    public void GeneratePathToTarget( Vector3 targetPosition )
    {
        currentWaypoint = 0;
        isAtEndOfPath = false;
        path = pathfindingSeeker.StartPath( transform.position, targetPosition );
    }

    public void StepTowardsTarget()
    {
        if (path == null)
        {
            //We have no path to move after yet
            return;
        }

        if (currentWaypoint >= path.vectorPath.Count)
        {
            //Debug.Log("End Of Path Reached");
            isAtEndOfPath = true;
            return;
        }

        // Set the rotation.
        Vector3 moveDir = Vector3.zero;
        float zRotation = MathUtilities.FullRangeRotationFromOriginToTarget(
            transform.position,
            path.vectorPath[currentWaypoint],
            out moveDir );
        transform.rotation = Quaternion.Euler(0,0,zRotation);
        
        // Move in direction to the next waypoint
        transform.position += moveDir * speed * Time.fixedDeltaTime;

        //Check if we are close enough to the next waypoint
        //If we are, proceed to follow the next waypoint
        if (Vector3.Distance(transform.position, path.vectorPath[currentWaypoint]) < nextWaypointDistance)
        {
            currentWaypoint++;
            return;
        }
    }

    public void StartContinuouslyTracking()
    {
        if (false == isContinuouslyStepping)
        {
            isContinuouslyStepping = true;
            StartCoroutine(ContinuouslyStepPath());
        }
    }

    public void StopContinuouslyTracking()
    {
        isContinuouslyStepping = false;
    }

    IEnumerator ContinuouslyStepPath()
    {
        while (isContinuouslyStepping)
        {
            StepTowardsTarget();
            yield return null;
        }
    }
}
