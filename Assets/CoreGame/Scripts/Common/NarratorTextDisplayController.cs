using UnityEngine;
using System.Collections;
using System;

/********************************************************************************************
 * The [NarratorTextDisplayController] uses the inspector-configured information
 * to notify the [NarratorTextDisplay] which strings from the [NarratorTextBank] to present.
 ********************************************************************************************/
public class NarratorTextDisplayController : MonoBehaviour
{
    private NarratorTextBank textBank;
    private NarratorTextDisplay textDisplay;

    // The sequence of ID's to be used for the dialog
    [SerializeField]
    private string[] textDisplayIdSequence;
    private int curIdSequenceIndex;

    [SerializeField]
    private bool awaitInputToContinue;

    public event Action SequenceCompletedEvent;
    void Awake()
    {
        textBank = GetComponent<NarratorTextBank>();
        textDisplay = GetComponent <NarratorTextDisplay>();
        textDisplay.StreamCharactersFinishEvent += OnStreamCharactersFinishEvent;
    }

    void OnStreamCharactersFinishEvent()
    {
        //If we finished the last sequence (we are in the final index)...
        if (curIdSequenceIndex == textDisplayIdSequence.Length - 1)
        {
            //Fire the Sequence completed event.
            if(SequenceCompletedEvent != null)
            {
                SequenceCompletedEvent();
            }
        }
        
        if (false == awaitInputToContinue)
        {
            ContinueSequence();
        }
    }

    public void DisplaySequence()
    {
        DisplaySequence(0);
    }

    /// <summary>
    /// Skips onto the next dialogue in the sequence.
    /// </summary>
    /// <returns>Is the sequence still running? Are we still within bounds?</returns>
    public bool ContinueSequence()
    {
        curIdSequenceIndex++;

        if (curIdSequenceIndex < textDisplayIdSequence.Length )
        {
            DisplaySequence(curIdSequenceIndex);
            return true;
        }

        return false;
    }

    private void DisplaySequence( int sequenceIndex )
    {
        string narratorLine = textBank.GetNarratorLine(textDisplayIdSequence[sequenceIndex]);
        textDisplay.StreamCharacters( narratorLine );
    }
}
