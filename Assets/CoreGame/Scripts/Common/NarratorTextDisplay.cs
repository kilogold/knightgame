using UnityEngine;
using System.Collections;
using System;

public class NarratorTextDisplay : MonoBehaviour
{
    private TextMesh textMeshDisplay;
    
    [SerializeField]
    private float letterDelayInSeconds;
    
    [SerializeField]
    private int newLineCharacterLimit;

    private bool isStreamingCharacters;

    public event Action StreamCharactersFinishEvent;

    // Use this for initialization
    void Awake()
    {
        textMeshDisplay = GetComponent<TextMesh>();
        textMeshDisplay.GetComponent<Renderer>().sortingLayerName = "TextOverlay";
    }

    public void StreamCharacters(string textToDisplay)
    {
        if(isStreamingCharacters)
        {
            StopCoroutine("StreamCharactersCoroutine");
        }
            
        StartCoroutine("StreamCharactersCoroutine",textToDisplay);
    }

    private IEnumerator StreamCharactersCoroutine( string textToDisplay )
    {
        isStreamingCharacters = true;

        textMeshDisplay.text = string.Empty;

        foreach (char curLetter in textToDisplay)
        {
            textMeshDisplay.text += curLetter;
            
            // We don't want to delay for whitespace.
            if (char.IsWhiteSpace(curLetter))
            {
                continue;
            }
            else
            {
                yield return new WaitForSeconds(letterDelayInSeconds);
            }
        }

        isStreamingCharacters = false;

        if (StreamCharactersFinishEvent != null)
        {
            StreamCharactersFinishEvent();
        }
    }
}
