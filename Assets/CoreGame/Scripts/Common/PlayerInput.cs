﻿using UnityEngine;
using System.Collections;

public class PlayerInput : MonoBehaviour 
{
	public float SPEED = 10;
	public LevelLogic levelLogicRef;
	public GameObject swordGameObject;
    public Animator playerAnimator;
    public ActorAnimations playerAnimations;
	
	// Update is called once per frame
	void Update () 
	{
        // Only process input if we are NOT striking.
        // We don't want to hover as we strike.
        if (ActorAnimations.ActorAnimationState.Striking != playerAnimations.CurrentAnimationState)
        {
            float vMove = Input.GetAxis("Vertical") * SPEED;
            float hMove = Input.GetAxis("Horizontal") * SPEED;
            Vector2 moveDir = new Vector2(hMove, vMove);
            GetComponent<Rigidbody2D>().AddForce(moveDir);

            //Only if there has been input...
            if (false == Mathf.Approximately(moveDir.magnitude, 0))
            {
                playerAnimations.SetWalkAnimation(moveDir);
            }
            else
            {
                playerAnimations.SetIdleAnimation();
            }

            if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.JoystickButton0))
            {
                playerAnimations.SetStrikingAnimation();
            }
        }
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		// If we are not at the final map AND 
        // If we are not transitioning AND
        // the trigger is named LevelBorder...
		if( levelLogicRef.DoesNextMapExist() &&
            levelLogicRef.isTransitioningToNextMap == false &&
            other != null &&
            other.gameObject.name == "LevelBorder" )
		{
			levelLogicRef.AdvanceToNextMap();
		}
	}
}
