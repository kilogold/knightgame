﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Sword behavior.
/// Only executes once and automatically disables.
/// </summary>
public class SwordBehavior : MonoBehaviour 
{

	public float stabSpeed;
	public float stabDistance;
	public float currentSwordLerp;
	private Vector3 originalPosition;
	private Vector3 finalPosition;
	private sbyte moveDirection;

    
	// Use this for initialization
	void Start () 
	{
		originalPosition = transform.localPosition; 
		finalPosition = new Vector3( originalPosition.x, originalPosition.y - stabDistance, originalPosition.z );
		moveDirection = -1;
		currentSwordLerp = 0;
	}
	
	// Update is called once per frame
	void Update () 
	{
		currentSwordLerp += (stabSpeed * Time.deltaTime) * moveDirection;

		if( currentSwordLerp > 1 )
		{
			currentSwordLerp = 1;
			moveDirection = -1;
		}
		else if( currentSwordLerp < 0 )
		{
			currentSwordLerp = 0;
			moveDirection = 1;
			gameObject.SetActive( false );
		}

		transform.localPosition = Vector3.Lerp(originalPosition,finalPosition, currentSwordLerp);
	}

	void OnDisable()
	{
		transform.localPosition = originalPosition;
	}
}
