﻿using UnityEngine;
using System.Collections;

public class CameraAdvance : MonoBehaviour 
{
    public Transform nextMap;
    private bool isCoroutineRunning = false;

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (false == isCoroutineRunning)
        {
            StartCoroutine(OffsetCameraCoroutine());
        }
    }

    private IEnumerator OffsetCameraCoroutine()
    {
        isCoroutineRunning = true;
        {
            float transitionLerp = 0;
            Vector3 lerpFrom = transform.position;
            Vector3 lerpTo = nextMap.position;

            while (transitionLerp < 1)
            {
                transitionLerp += Time.deltaTime;
                transform.position = Vector3.Lerp(lerpFrom, lerpTo, transitionLerp);
                yield return null;
            }

            transform.position = lerpTo;
        }
        isCoroutineRunning = false;
    }
}
