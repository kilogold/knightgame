﻿using UnityEngine;
using System.Collections;

public class DubiousFriendAnimations : ActorAnimations 
{
    void Start()
    {
        lastFacingDirection = CoreDeclarations.DIRECTIONS.DOWN;
        InitializeAnimationHashContainer((int)ActorAnimationState.MAX_STATES);

        SetAnimationHash(ActorAnimationState.Walking, CoreDeclarations.DIRECTIONS.LEFT, "Base Layer.DubiousFriendWalkLeft");
        SetAnimationHash(ActorAnimationState.Walking, CoreDeclarations.DIRECTIONS.DOWN, "Base Layer.DubiousFriendWalkDown");
        SetAnimationHash(ActorAnimationState.Walking, CoreDeclarations.DIRECTIONS.UP, "Base Layer.DubiousFriendWalkUp");
        SetAnimationHash(ActorAnimationState.Walking, CoreDeclarations.DIRECTIONS.RIGHT, "Base Layer.DubiousFriendWalkRight");

        SetAnimationHash(ActorAnimationState.Idle, CoreDeclarations.DIRECTIONS.LEFT, "Base Layer.DubiousFriendIdleLeft");
        SetAnimationHash(ActorAnimationState.Idle, CoreDeclarations.DIRECTIONS.DOWN, "Base Layer.DubiousFriendIdleDown");
        SetAnimationHash(ActorAnimationState.Idle, CoreDeclarations.DIRECTIONS.UP, "Base Layer.DubiousFriendIdleUp");
        SetAnimationHash(ActorAnimationState.Idle, CoreDeclarations.DIRECTIONS.RIGHT, "Base Layer.DubiousFriendIdleRight");

        SetAnimationHash(ActorAnimationState.Striking, CoreDeclarations.DIRECTIONS.LEFT, "Base Layer.DubiousFriendStrikeLeft");
        SetAnimationHash(ActorAnimationState.Striking, CoreDeclarations.DIRECTIONS.DOWN, "Base Layer.DubiousFriendStrikeDown");
        SetAnimationHash(ActorAnimationState.Striking, CoreDeclarations.DIRECTIONS.UP, "Base Layer.DubiousFriendStrikeUp");
        SetAnimationHash(ActorAnimationState.Striking, CoreDeclarations.DIRECTIONS.RIGHT, "Base Layer.DubiousFriendStrikeRight");
    }
}
