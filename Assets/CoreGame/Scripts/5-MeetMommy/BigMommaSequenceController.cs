﻿using UnityEngine;
using System.Collections;
using WellFired;

public class BigMommaSequenceController : MonoBehaviour
{
    [SerializeField]
    private USSequencer babiesEscapeSequence;

    [SerializeField]
    private USSequencer bigMommaIntroSequence;

    [SerializeField]
    private USSequencer friendRunawaySequence;

    [SerializeField]
    private USTimelineObjectPath friendRunawayObjectPath;

    [SerializeField]
    private GameObject friendRunawayProp;

    private LevelLogic levelLogicRefGame;
    private PlayerInput playerInputRef;

    void Start()
    {

        babiesEscapeSequence.PlaybackFinished += OnPlaybackFinished;
        bigMommaIntroSequence.PlaybackFinished += OnPlaybackFinished;
        friendRunawaySequence.PlaybackFinished += OnPlaybackFinished;
    }

    void OnDestroy()
    {
        babiesEscapeSequence.PlaybackFinished -= OnPlaybackFinished;
        bigMommaIntroSequence.PlaybackFinished -= OnPlaybackFinished;
        friendRunawaySequence.PlaybackFinished -= OnPlaybackFinished;
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            // Position friend prop where the actual friend instance is and swap out.
            Transform dubiousFriendInstanceTransform = GameObject.FindObjectOfType<DubiousFriendBehavior>().transform;
            dubiousFriendInstanceTransform.gameObject.SetActive(false);
            friendRunawayObjectPath.Keyframes[0].Position = dubiousFriendInstanceTransform.position;
            friendRunawayProp.transform.position = dubiousFriendInstanceTransform.position;
            friendRunawayProp.SetActive(true);

            //Freeze player input
            playerInputRef = other.GetComponent<PlayerInput>();
            playerInputRef.enabled = false;
            
            // Don't run the collision event anymore
            Destroy(GetComponent<Collider2D>());

            babiesEscapeSequence.Play();
            return;
        }
    }

    private void OnPlaybackFinished(USSequencer sequence)
    {
        if (sequence == babiesEscapeSequence)
        {
            bigMommaIntroSequence.Play();
        }
        else if( sequence == bigMommaIntroSequence)
        {
            friendRunawaySequence.Play();
        }
        else if( sequence == friendRunawaySequence)
        {
            playerInputRef.enabled = true;
        }
        else
        {
            Debug.Log("Unknown sequence...");
        }
    }

}
