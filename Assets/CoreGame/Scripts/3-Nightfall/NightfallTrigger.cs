﻿using UnityEngine;
using System.Collections;
using WellFired;

public class NightfallTrigger : MonoBehaviour 
{
	public GameObject levelPieceToReplace;
    private LevelLogic levelLogicRef;

    void Start()
    {
        levelLogicRef = GameObject.FindObjectOfType<LevelLogic>();
    }

	void OnTriggerEnter2D(Collider2D other)
	{
        //The player has reached the trigger
        GameObject collidingObject = other.gameObject;
		if( collidingObject.tag == "Player" )
        {
            GameObject girlThiefObject = GameObject.FindGameObjectWithTag("GirlThief");
            
            //Ensure the girl object is not in the transitional objects
            levelLogicRef.transitioningObjects.Remove(girlThiefObject.transform);            
                
            //Destroy the girl object
            if (girlThiefObject != null)
            {
                Destroy(girlThiefObject);
            }



            //Begin the cutscene
            Application.LoadLevelAdditive("LevelFloorBG3-Cut");
			StartCoroutine( BeginSequence() );
        }
    }

	IEnumerator BeginSequence()
	{   
        //wait until next frame to give the additive level time 
        //to instantiate & initialize.
		yield return null;

        //Obtain the new levelpiece in the scene
        GameObject newLevelPiece = GameObject.FindGameObjectWithTag("LevelPieceReplacement");

		//Grab hold of the sequence
		GameObject sequenceObject = GameObject.FindGameObjectWithTag("uSequence");
		USSequencer incomingSequence = sequenceObject.GetComponent<USSequencer>();

		//sequence to play
        incomingSequence.Play();

        // Transfer persistent GameObjects from the replaced level piece
        // to the new level piece.
        foreach (Transform curTransform in levelLogicRef.transitioningObjects)
        {
            curTransform.parent = newLevelPiece.transform;
        }

		//Remove current level piece to allow smooth sequence playing.
		//The incoming sequence already has this object, we don't want duplicates.
		Destroy(levelPieceToReplace);

        //Assign the incoming level piece as the current level piece
        levelLogicRef.currentMap = newLevelPiece;
    }
}

