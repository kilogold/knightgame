﻿using UnityEngine;using System.Collections;public class DayNightTinter : MonoBehaviour{    public SpriteRenderer backgroundToTint;    public SpriteRenderer[] foregroundToTint;    public Color backgroundTint;    public Color foregroundTint;	// Use this for initialization	void Start () {		}		// Update is called once per frame	void Update () {		}    public void SetDayTint()    {
        backgroundToTint.color = Color.white;

        foreach (SpriteRenderer curForegroundItem in foregroundToTint)
        {
            curForegroundItem.color = Color.white;
        }
    }    public void SetNightTint()    {
        backgroundToTint.color = backgroundTint;

        foreach ( SpriteRenderer curForegroundItem in foregroundToTint )
        {
            curForegroundItem.color = foregroundTint;
        }
    }}