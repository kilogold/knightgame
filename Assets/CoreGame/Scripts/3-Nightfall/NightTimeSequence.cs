﻿using UnityEngine;
using System.Collections;

public class NightTimeSequence : MonoBehaviour
{
    public GameObject playerDummy;
    public GameObject girlThiefDummy;
    public DayNightTinter dayNightTinter;
    public GameObject nightSequence;
    public GameObject dayAfterSequence;

    // Use this for initialization
    void Start()
    {
        StartCoroutine( ExecuteSequence() );
    }

    // Update is called once per frame
    void Update()
    {

    }

    private IEnumerator ExecuteSequence()
    {
        // Move towards the girl
        yield return new WaitForSeconds(3);
        dayNightTinter.SetDayTint();

        //Transition to the next sequence: The day after.
        nightSequence.SetActive(false);
        dayAfterSequence.SetActive(true);

        //Move the dummy to the next sequence
        playerDummy.transform.parent = dayAfterSequence.transform;

    }
}
