﻿using UnityEngine;
using System.Collections;
using WellFired;

public class NightfallSequenceObjectAssociator : MonoBehaviour 
{
	public USTimelineContainer playerTimeline;


	// Use this for initialization
	void Start () 
	{
		playerTimeline.AffectedObject = GameObject.FindGameObjectWithTag("Player").transform;
		Destroy(this);
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
}
