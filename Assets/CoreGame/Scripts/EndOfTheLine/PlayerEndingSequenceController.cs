﻿using UnityEngine;
using System.Collections;
using WellFired;
public class PlayerEndingSequenceController : MonoBehaviour
{
    [SerializeField]
    private USSequencer chainedSequenceStart;
    private LevelLogic levelLogicRef;

    // Use this for initialization
    void Start()
    {
        levelLogicRef = GameObject.FindObjectOfType<LevelLogic>();

        levelLogicRef.LevelPieceTransitionFinish += OnLevelTransitionFinish;
    }

    void OnLevelTransitionFinish()
    {
        levelLogicRef.LevelPieceTransitionFinish -= OnLevelTransitionFinish;

        GameObject playerRef = GameObject.FindGameObjectWithTag("Player");
        Destroy(playerRef);

        chainedSequenceStart.Play();
    }
}
