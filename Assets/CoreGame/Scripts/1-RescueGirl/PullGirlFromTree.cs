﻿using UnityEngine;
using System.Collections;

public class PullGirlFromTree : MonoBehaviour 
{
	private GameObject girlReference;

	// Use this for initialization
	void Start () 
	{
		girlReference = GameObject.FindWithTag( "GirlThief" );
	}
	
	// Update is called once per frame
	void Update () 
	{
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if( other.gameObject.tag == "Player" )
		{
			GameObject logicObject = GameObject.Find( "LevelLogic" );
			logicObject.GetComponent<LevelLogic>().transitioningObjects.Add( girlReference.transform );
			Destroy( girlReference.GetComponent<GirlStuckInTree>() );
			girlReference.transform.localPosition += new Vector3( -2.0f, 0, 0);
			girlReference.transform.parent = null;
			girlReference.AddComponent<FollowHero>();
			Destroy( gameObject );
        }
    }
}
