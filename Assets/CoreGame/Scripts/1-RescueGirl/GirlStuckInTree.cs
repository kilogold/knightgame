﻿using UnityEngine;
using System.Collections;

public class GirlStuckInTree : MonoBehaviour 
{
	/// <summary>
	/// The lerp move direction (towards 1 or 0)
	/// </summary>
	public sbyte moveDirection = 1;

	/// <summary>
	/// The lerp value
	/// </summary>
	public float twitchTweenLerp = 0.0f;

	/// <summary>
	/// The twitch magnitude.
	/// </summary>
	public float twitchMagnitude = 5.0f;

	/// <summary>
	/// The twitch speed.
	/// </summary>
	public float twitchSpeed = 1.5f;

	/// <summary>
	/// The max delay between twitch.
	/// </summary>
	public float maxDelayBetweenTwitch = 2.0f;

	/// <summary>
	/// The current timer for delay between twitch.
	/// </summary>
	public float currentDelayBetweenTwitch = 0.0f;
    

	public Vector3 originalPosition;
	public Vector3 twitchPostion;

	// Use this for initialization
	void Start () 
	{
		originalPosition = transform.localPosition;
		GenerateTwitchPosition();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if( currentDelayBetweenTwitch <= 0 )
		{
			// Tick the tween value
			twitchTweenLerp += twitchSpeed * Time.deltaTime * moveDirection;

			// Manage lerp values for twitching
			if( twitchTweenLerp > 1 )
			{
				twitchTweenLerp = 1;
				moveDirection = -1;
			}
			else if( twitchTweenLerp < 0 )
			{
				twitchTweenLerp = 0;
				moveDirection = 1;

				//We are done with this twitch.
				//Generate the next position, and wait for the timer.
				GenerateTwitchPosition();

				//Reset the timer.
				currentDelayBetweenTwitch = maxDelayBetweenTwitch;
	        }

			// Perform the twitch
			transform.localPosition = Vector3.Lerp( originalPosition, twitchPostion, twitchTweenLerp );
		}
		else
		{
			currentDelayBetweenTwitch -= Time.deltaTime;
		}
	}

	private void GenerateTwitchPosition()
	{
		twitchPostion = new Vector3(
			Random.Range(0,twitchMagnitude),
			Random.Range(0,twitchMagnitude),
			Random.Range(0,twitchMagnitude) );
	}
}
